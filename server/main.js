import { Meteor } from 'meteor/meteor';
import { Links } from '../imports/collections/links';
import { WebApp} from 'meteor/webapp';
import ConnectRoute from 'connect-route';

Meteor.startup(() => {
    Meteor.publish('links', function() {
       return Links.find({});
    });
});

//executed whenever a user visits with a route like
//localhost:3000/adfv
function onRoute(req, res, next) {

    //Take the token out of the url and try a find a
    //matching link in the links collection

    const link = Links.findOne({token: req.params.token});

    if(link){
        //if we find a link object, redirect the user to the
        //long url
        Links.update(link, {$inc: {clicks:  1}});
        res.writeHead(307, { 'Location': link.url} );
        res.end();
    }else {
        //if we dont find a link object, send the user
        //to our nomrla react app
        next();
    }
}

const middlewate = ConnectRoute(function(router){
   router.get('/:token', onRoute);
});

WebApp.connectHandlers.use(middlewate);
