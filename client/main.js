import React from 'react';
import ReactDOM from 'react-dom';

import Header from './components/header';
import CreateLink from './components/linkCreate';
import { Links } from '../imports/collections/links';
import LinkList from './components/linkList';


const App = () => {
  return (
      <div>
          <Header />
          <CreateLink />
          <LinkList />
      </div>
  );
};

Meteor.startup(() => {
  ReactDOM.render(<App />, document.querySelector('.container'));
});