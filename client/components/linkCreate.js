import React, { Component } from 'react';

class CreateLink extends Component {

    constructor(props){
        super(props);

        this.state = { error: '' };
    }

    handleSubmit(event){
        event.preventDefault();
        Meteor.call('links.insert', this.refs.link.value, (error) => {
            if(error)
                this.setState({ error: 'Enter a valid URL' });
            else{
                this.setState({ error: '' });
                this.refs.link.value = '';
            }
        });
    }

    render() {
        return(
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="form-group">
                    <p><label>Link to shorten</label></p>
                    <input ref="link" className="from-control" type="text"/>
                </div>
                <div className="text-danger">{this.state.error}</div>
                <button className="btn btn-primary">Shorten!</button>
            </form>
        );
    }
}

export default CreateLink;